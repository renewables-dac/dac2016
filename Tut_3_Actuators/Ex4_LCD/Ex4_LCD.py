#Example 3.4 16x2 LCD Display

#Uses the LCD from the Arduino Projects Kit
#See tutorial 3.4 for wiring diagram

#If you want to see more about how this works, use nano to view the LCD_Module.py script


import LCD_Module as LCD

line1=raw_input("Line 1 text: ")
line2=raw_input("Line 2 text: ")

LCD.lcd_init()
LCD.lcd_string(line1,LCD.LCD_LINE_1)
LCD.lcd_string(line2,LCD.LCD_LINE_2)

