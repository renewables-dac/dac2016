#Example 3.2 Servo Motor for Raspberry Pi using Pulse Width Modulation

#This is an example of PWM implementation using software.
#Other processes running may cause jitters in motor operation


#Ideally, servo motor control is implemented using hardware PWM (Covered in next example)

#Servo motor operates on a 20ms pulse period with 0 deg = 0.0005s, 180 deg =0.0025s pulse widths (for MG996R)

#Build the circuit in tutorial Example 3.2 


import RPi.GPIO as GPIO #import GPIO module
import time
GPIO.cleanup()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)      #BCM uses Broadcom pin numbering
GPIO.setup(18,GPIO.OUT)     #define pin 18 as output pin



angle = float(input("What angle do you require? (0 to 180 Deg) "))
pulsewidth=(0.0005+(angle/180)*0.002)

count=0
while count<100:
    GPIO.output(18,GPIO.HIGH)   # Set pin 18 High, (1 or True) to 3.3V
    time.sleep(pulsewidth)


    GPIO.output(18,GPIO.LOW)    # Set pin 18 Low, (0 or False) to 0V
    time.sleep(0.02-pulsewidth)
    count=count+1



