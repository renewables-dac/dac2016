#!/usr/bin/env python

#This script is for controlling larger servos such as the Parallax standard servo or Tower Pro MG996R
#Beware of the wiring - Servos usually have a red wire for Vcc and an orange or yellow wire for the pulse input. 
#The remaining wire, usually black or brown is ground. 


#At 1500uS, or 1.5ms the servo is set to its central position. Servos do not all have the same range of travel.
#When using an unfamiliar servo, start at the central position, then gradually build the range outwards until the motor finds its limit.
#At the limit, the motor will start to judder, and going beyond this point can damage the servo motor

import time
 
import pigpio
 
servos = 18 #GPIO number
pi = pigpio.pi() 
#pigpio.start()
#pulsewidth can only set between 500-2500


try:
    while True:
        pi.set_servo_pulsewidth(servos, 500) #0 degree
        print("Servo {} {} micro pulses".format(servos, 1000))
        time.sleep(1)
        pi.set_servo_pulsewidth(servos, 1500) #90 degree
        print("Servo {} {} micro pulses".format(servos, 1500))
        time.sleep(1)
        pi.set_servo_pulsewidth(servos, 2360) #180 degree
        print("Servo {} {} micro pulses".format(servos, 2000))
        time.sleep(1)
        pi.set_servo_pulsewidth(servos, 1500)
        print("Servo {} {} micro pulses".format(servos, 1500))
        time.sleep(1)
 
   # switch all servos off
except KeyboardInterrupt:
    pi.set_servo_pulsewidth(servos, 0);
 
pi.stop()
